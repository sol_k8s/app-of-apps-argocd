# AIDE MEMOIRE Kubernetisation

## Creation d'un helm chart

On veut déployer une application avec deux services :

* un front basé sur l'image registry.forgemia.inra.fr/machin/front/machin-front:0.5 et servi à l'adresse machin.gissol.fr/

* un back basé sur l'image registry.forgemia.inra.fr/machin/back/machin-back:0.5 et servi à l'adresse machin.gissol.fr/api/

1. Créer un chart "chapeau" :

   ```bash
   helm create projet0
   ```

2. Se déplacer dans le dossier créé.
3. Supprimer le dossier template de notre helm chart. Vider le fichier values.yaml.
4. Se déplacer dans le dossier "charts"

   ```bash
   cd charts
   ```

5. Créer deux "subcharts" : un chart pour le front, un chart pour le back.
  
    ```bash
    helm create front
    helm create back
    ```

    On a alors la structure suivante qu'il va falloir paramétrer :

    ```bash
    .
    ├── charts
    │   ├── back
    │   │   ├── charts
    │   │   ├── Chart.yaml
    │   │   ├── templates
    │   │   │   ├── deployment.yaml
    │   │   │   ├── _helpers.tpl
    │   │   │   ├── hpa.yaml
    │   │   │   ├── ingress.yaml
    │   │   │   ├── NOTES.txt
    │   │   │   ├── serviceaccount.yaml
    │   │   │   ├── service.yaml
    │   │   │   └── tests
    │   │   │       └── test-connection.yaml
    │   │   └── values.yaml  # values back (à paramétrer)
    │   └── front
    │       ├── charts
    │       ├── Chart.yaml
    │       ├── templates
    │       │   ├── deployment.yaml
    │       │   ├── _helpers.tpl
    │       │   ├── hpa.yaml
    │       │   ├── ingress.yaml
    │       │   ├── NOTES.txt
    │       │   ├── serviceaccount.yaml
    │       │   ├── service.yaml
    │       │   └── tests
    │       │       └── test-connection.yaml
    │       └── values.yaml  # values front (à paramétrer)
    ├── Chart.yaml
    └── values.yaml  #values racine (à vider)
    ```

6. Ensuite on paramètre les values du front et du back.

    *Remarque :*  On peut surcharger les valeurs du front ou du back dans le values racine de la manière suivante :

    ```yaml
    --- values.yaml racine

    front: # Si on veut surcharger les valeurs du values.yaml du front
    replicaCount: 1
    
    image:
        repository: CE Qu'on veut
        pullPolicy: IfNotPresent
        # Overrides the image tag whose default is the chart appVersion.
        tag: ""
        
    ```

7. Versionner le tout dans un depot "nom-appli-helm" du projet "nom-appli"

## Variables d'environnement

Dans le fichier deployments.yaml, ajouter à la section containers une section env :

```yaml
      containers:
          ...
          env:
            {{- range $key, $val := .Values.env }}
            - name: {{ $key }}
              value: "{{ $val }}"
            {{- end}}
```

Puis dans le fichier values.yaml ajouter les variables d'environnement :

```config
env:
  EXEMPLE: "test de variable d'environnement"
```

## Sealed Secrets

1. Installer et configurer sealedsecrets (cf [https://forgemia.inra.fr/sol_k8s/infrastructure](https://forgemia.inra.fr/sol_k8s/infrastructure))
2. `export SEALED_SECRETS_CERT="chemin_absolu_vers_fichier_kubeseal_cert.cert"` ou encore :`export SEALED_SECRETS_CERT="https://sealed-secret.dev.gissol.fr/v1/cert.pem"`
3. Créer un fichier mon-secret.yml dans le dossier templates avec par exemple en faisant :

   ```bash
    kubectl create secret generic mysecret --dry-run=client --from-literal=cleExemple=mot_de_passe -o yaml > mon-secret.yml
   ```

   Le fichier ressemble alors à :

   ```yaml
   apiVersion: v1
   data:
     cleExemple: bW90X2RlX3Bhc3Nl
   kind: Secret
   metadata:
      creationTimestamp: null
      name: mysecret
   ```

   Pour l'éditer facilement, on peut remplacer data par stringData :

   ```yaml
   apiVersion: v1
   stringData:
     cleExemple: mot_de_passe
   kind: Secret
   metadata:
      creationTimestamp: null
      name: mysecret
   ```

4. Encoder le fichier YAML avec kubeseal :

   ```bash
   kubeseal -f mon-secret.yml > mon-secret-sealed.yml
   ```

5. Supprimer le fichier mon-secret.yml
6. Ajouter dans le fichier deployments.yaml les lignes permettant de faire appel au secret :

   ```yaml
      containers:
          ...
          envFrom:
            - secretRef:
                name: mysecret
    ```


