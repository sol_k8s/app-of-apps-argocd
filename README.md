# App of Apps des application du SI Sol kubernétisées

Ce dépôt contient toutes les applications du SI sol qui ont été kubernétisées. Il est géré par un administrateur système. Ce README vous aidera à déployer ces applications en utilisant le modèle "App of Apps" avec Argo CD.

## Utilisation

### Prérequis

- Argo CD doit être déjà installé sur votre cluster Kubernetes.
- Vous devez avoir les privilèges administrateur pour créer et gérer les applications dans Argo CD.

### Structure du Dépôt

La structure typique du dépôt Git pour ce modèle est la suivante :

```bash
├── Chart.yaml
├── templates
│   ├── app1.yaml
│   ├── app2.yaml
│   ├── app3.yaml
│   └── appN.yaml
└── values.yaml
```

- **Chart.yaml** : Fichier boiler-plate pour Helm.
- **templates/** : Contient un fichier pour chaque application enfant.
- **values.yaml** : Contient les valeurs par défaut pour les applications.

#### Exemple de fichier `app.yaml` pour une application enfant

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app1
  namespace: argocd ## BIEN GARDER le namespace argocd ici
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: monappli-dev ## BIEN METTRE LE namespace où on veut que notre application soit
    server: {{ .Values.spec.destination.server }}
  project: default
  source:
    path: app1
    repoURL: https://github.com/votre-org/votre-repo.git
    targetRevision: HEAD
  syncPolicy:
    automated:
      prune: true
```

#### Fichier `values.yaml`

```yaml
spec:
  destination:
    server: https://kubernetes.default.svc
```

### Déploiement via la Ligne de Commande

Pour créer et synchroniser l'application parent, utilisez les commandes suivantes :

```sh
argocd app create apps \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://forgemia.inra.fr/sol_k8s/app-of-apps-argocd.git \
    --path apps  

argocd app sync apps
```

Pour synchroniser les applications enfants via la ligne de commande :

```sh
argocd app sync -l app.kubernetes.io/instance=apps
```

### Déploiement via l'Interface Web

1. **Créer l'Application Parent** :
    - Allez dans l'interface web d'Argo CD.
    - Cliquez sur "NEW APP".
    - Remplissez les champs nécessaires avec les informations suivantes :
        - **Application Name** : `apps`
        - **Project** : `default`
        - **Sync Policy** : `Manual`
        - **Repository URL** : `https://forgemia.inra.fr/sol_k8s/app-of-apps-argocd.git`
        - **Revision** : `HEAD`
        - **Path** : `.`
        - **Destination Server** : `https://kubernetes.default.svc`
        - **Destination Namespace** : `argocd`
    - Cliquez sur "Create".

2. **Synchroniser les Applications Enfants** :
    - Dans l'interface web, filtrez les applications avec le label correct :
        - **Label** : `app.kubernetes.io/instance=apps`
    - Sélectionnez les applications "out of sync" et cliquez sur "SYNC".

### Suppression en Cascade

Pour s'assurer que les applications enfants et toutes leurs ressources sont supprimées lorsque l'application parent est supprimée, ajoutez le finalizer approprié dans votre définition d'application :

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app1
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  ...
```

### Références

- [Argo CD Documentation](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/)

## Credentials forgemia

Le fichier private-repo-forgemia-creds-sealed.yaml contient le secret permettant de se connecter aux dépots sous <https://forgemia.inra.fr/infosol-sisol/webapps/>, c'est un "credential template" (<https://argo-cd.readthedocs.io/en/release-2.0/user-guide/private-repositories/#credential-templates>) .
Pour le créer, créer un token sur forgemia, puis :

```yaml
apiVersion: v1
stringData:
  password: LE_TOKEN
  type: git
  url: https://forgemia.inra.fr/infosol-sisol/webapps/
  username: LE_NOM_DUSER
kind: Secret
metadata:
  creationTimestamp: null
  labels:
    argocd.argoproj.io/secret-type: repo-creds
  name: private-repo-forgemia-creds
  namespace: argocd
```

et :

```bash
kubeseal -f private-repo-forgemia-creds.yaml > private-repo-forgemia-creds-sealed.yaml
```

## Divers

### Connexion à argocd

Pour rappel, la connexion à argocd est décrite dans le README du dépot forgemia.inra.fr:sol_k8s/infrastructure.git


### Configuration Sonarqube

Une fois installé, dans sonarqube il faut :

* Se connecter et modifier le mot de passe admin
* activer la connexion à forgemia en suivant cette documentation : https://docs.sonarsource.com/sonarqube-server/9.9/instance-administration/authentication/gitlab/ . Dans sonarqube, il faudra renseigner : Gitlab URL : https://forgemia.inra.fr/, APP ID (ce qu'aura donné forgemia), APP Secret (idem), Cocher la case "synchronizer les groupes" et la case "allow users to sign up".
* Créer les groupes manuellement dans sonarque pour refléter ceux de forgemia (typiquement : infosols-sisol/webapps)
